<?php

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;


return function (ContainerConfigurator $containerConfigurator, ContainerBuilder $containerBuilder) {

    \App\Services::configureServices($containerConfigurator, $containerBuilder);

};


