<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class OlxOfferLink extends Constraint
{

    public $offerLinkBase = ['https://www.olx.ua/obyavlenie/', 'https://m.olx.ua/obyavlenie/'];

    public $protocols = ['https'];

}
