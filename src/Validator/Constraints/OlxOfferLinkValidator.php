<?php


namespace App\Validator\Constraints;


use App\Component\ProductParser\OlxCurl;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OlxOfferLinkValidator extends ConstraintValidator
{


    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var OlxCurl
     */
    private $olxCurl;

    public function __construct(ValidatorInterface $validator, OlxCurl $olxCurl)
    {
        $this->validator = $validator;
        $this->olxCurl = $olxCurl;
    }


    /**
     * @inheritDoc
     */
    public function validate($value, Constraint $constraint)
    {

        if (!$constraint instanceof OlxOfferLink) {
            throw new UnexpectedTypeException($constraint, OlxOfferLink::class);
        }
        /**
         * @var ConstraintViolationInterface[] $constraints
         */
        $constraints = $this->validator->validate($value, [new Url(['protocols' => $constraint->protocols])]);
         if (count($constraints)){
            foreach ($constraints as $c){
                $this->context
                    ->buildViolation($c->getMessage())
                    ->setParameters($c->getParameters())
                    ->setCode($c->getCode())
                    ->atPath($c->getPropertyPath())
                    ->addViolation();
            }
            return;
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(\is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedValueException($value, 'string');
        }

        $value = (string) $value;
        if ('' === $value) {
            return;
        }

        $bases = $constraint->offerLinkBase;

        $valid = false;
        foreach ($bases as $base){
            $strLenBase = strlen($base);

            if ($strLenBase >= strlen($value)){
                $this->buildViolation();
                return;
            }

            $baseFromVal = substr($value, 0, $strLenBase);
            if ($baseFromVal === $base){
                $valid = true;
            }

        }

        if (false === $valid) {
            $this->buildViolation();
        }

      //  dd($this->olxCurl->curl($value)->getCode());
        if (200 !== $this->olxCurl->curl($value)->getCode()){
            $this->buildViolation();
        }

    }

    private function buildViolation($message = 'This value is not a valid URL')
    {
        $this->context->buildViolation($message)
            ->addViolation();
    }
}
