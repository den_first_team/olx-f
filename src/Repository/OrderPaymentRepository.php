<?php

namespace App\Repository;

use App\Entity\OrderPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OrderPayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderPayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderPayment[]    findAll()
 * @method OrderPayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderPayment::class);
    }

    /**
     * @param OrderPayment $orderPayment
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(OrderPayment $orderPayment): void
    {
        $em = $this->getEntityManager();
        if (!$em->contains($orderPayment)){
            $em->persist($orderPayment);
        }
        $em->flush($orderPayment);
    }

}
