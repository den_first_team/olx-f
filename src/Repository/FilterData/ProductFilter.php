<?php


namespace App\Repository\FilterData;


use App\Entity\User;

class ProductFilter
{

    /**
     * @var User|null
     */
    private $manager;

    /**
     * @var string|null
     */
    private $sortById;

    /**
     * @return User|null
     */
    public function getManager(): ?User
    {
        return $this->manager;
    }

    /**
     * @param User|null $manager
     * @return ProductFilter
     */
    public function setManager(?User $manager): ProductFilter
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSortById(): ?string
    {
        return $this->sortById;
    }

    /**
     * @param string|null $sortById
     * @return ProductFilter
     */
    public function setSortById(?string $sortById): ProductFilter
    {
        $this->sortById = $sortById;
        return $this;
    }

}
