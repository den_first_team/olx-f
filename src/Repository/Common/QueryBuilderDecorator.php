<?php


namespace App\Repository\Common;


use App\Repository\Filter\DateFilter;
use App\Repository\Filter\RangeFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;

class QueryBuilderDecorator
{

    /**
     * @var int
     */
    private $parameterQ = 0;

    /**
     * @var string
     */
    private $paramName;

    /**
     * @var QueryBuilder
     */
    private $queryBuilder;

    /**
     * QueryBuilderDecorator constructor.
     * @param QueryBuilder $queryBuilder
     */
    public function __construct(QueryBuilder $queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
        $this->paramName = uniqid();
    }

    /**
     * @param string $searchString
     * @param array $fields
     * @return $this
     */
    public function search(string $searchString, array $fields): self
    {
        $exprLike = [];
        $expr = $this->queryBuilder->expr();
        $parameterName = $this->getParameterName();
        foreach ($fields as $k => $field) {
            $exprLike[] = $expr->like($field, sprintf(':%s', $parameterName));
        }
        $this->queryBuilder->andWhere($expr->orX(...$exprLike))->setParameter($parameterName, '%' . $searchString . '%');
        return $this;
    }

    /**
     * @param string $field
     * @param $val
     * @return $this
     */
    public function equal(string $field, $val): self
    {
        $parameterName = $this->getParameterName();
        $this->queryBuilder
            ->andWhere(sprintf('%s = (:%s)', $field, $parameterName))
            ->setParameter($parameterName, $val);
        return $this;
    }

    /**
     * @param string $field
     * @param iterable $val
     * @param bool $notAddIfEmpty
     * @return $this
     */
    public function in(string $field, iterable $val, $notAddIfEmpty = true): self
    {
        $items = [];
        foreach ($val as $item){
            $items[] = $item;
        }
        if (true === $notAddIfEmpty && [] === $items){
            return $this;
        }
        $parameterName = $this->getParameterName();
        $this->queryBuilder
            ->andWhere(sprintf('%s IN (:%s)', $field, $parameterName))
            ->setParameter($parameterName, $items);
        return $this;
    }

    /**
     * @param string $field
     * @param string $string
     * @return $this
     */
    public function like(string $field, string $string): self
    {

        $parameterName = $this->getParameterName();
        $this->queryBuilder
            ->andWhere($field.' like :'.$parameterName)
            ->setParameter($parameterName, '%'.$string.'%');
        return $this;
    }

    /**
     * @param string $field
     * @param DateFilter $dateFilterData
     * @return $this
     * @throws \Exception
     */
    public function dateRange(string $field, DateFilter $dateFilterData): self
    {
        if (null !== $dateFilterData->getEqual()){
            $this->dateEqual($field, $dateFilterData->getEqual());
        }
        if (null !== $dateFilterData->getFrom()){
            $this->dateFrom($field, $dateFilterData->getFrom());
        }
        if (null !== $dateFilterData->getTo()){
            $this->dateTo($field, $dateFilterData->getTo());
        }
        return $this;
    }

    /**
     * @param string $field
     * @param RangeFilter $filterData
     * @return $this
     */
    public function floatRange(string $field, RangeFilter $filterData): self
    {
        if (null !== $filterData->getMax()){
            $parameterName = $this->getParameterName();
            $this->queryBuilder
                ->andWhere(sprintf('%s <= :%s', $field, $parameterName))
                ->setParameter($parameterName, $filterData->getMax());
            ;
        }
        if (null !== $filterData->getMin()){
            $parameterName = $this->getParameterName();
            $this->queryBuilder
                ->andWhere(sprintf('%s >= :%s', $field, $parameterName))
                ->setParameter($parameterName, $filterData->getMin());
            ;
        }
        return $this;
    }

    /**
     * @param string $fieldName
     * @param \DateTimeInterface $dateTime
     * @return $this
     * @throws \Exception
     */
    public function dateEqual(string $fieldName, \DateTimeInterface $dateTime): self
    {
        $from = new \DateTime($dateTime->format('Y-m-d') . ' 00:00:00');
        $to = new \DateTime($dateTime->format('Y-m-d') . ' 23:59:59');

        $parameterNameDataFrom = $this->getParameterName();
        $parameterNameDataTo = $this->getParameterName();

        $this->queryBuilder
            ->andWhere(sprintf('%s BETWEEN :%s AND :%s', $fieldName, $parameterNameDataFrom, $parameterNameDataTo))
            ->setParameter($parameterNameDataFrom, $from)
            ->setParameter($parameterNameDataTo, $to)
        ;
        return $this;
    }

    /**
     * @param string $fieldName
     * @param \DateTimeInterface $dateTime
     * @return $this
     * @throws \Exception
     */
    public function dateFrom(string $fieldName, \DateTimeInterface $dateTime): self
    {
        $from = new \DateTime($dateTime->format('Y-m-d') . ' 00:00:00');
        $parameterNameDataFrom = $this->getParameterName();
        $this->queryBuilder
            ->andWhere(sprintf('%s > :%s', $fieldName, $parameterNameDataFrom))
            ->setParameter($parameterNameDataFrom, $from);
        return $this;
    }

    /**
     * @param string $fieldName
     * @param \DateTimeInterface $dateTime
     * @return $this
     * @throws \Exception
     */
    public function dateTo(string $fieldName, \DateTimeInterface $dateTime): self
    {
        $to = new \DateTime($dateTime->format('Y-m-d') . ' 23:59:59');
        $parameterNameDataTo = $this->getParameterName();
        $this->queryBuilder
            ->andWhere(sprintf('%s < :%s', $fieldName, $parameterNameDataTo))
            ->setParameter($parameterNameDataTo, $to);
        return $this;
    }

    /**
     * @param $join
     * @param $alias
     * @param null $conditionType
     * @param null $condition
     * @param null $indexBy
     * @return $this
     */
    public function leftJoinSafe($join, $alias, $conditionType = null, $condition = null, $indexBy = null): self
    {

        $joinDqlParts = $this->queryBuilder->getDQLParts()['join'];
        foreach ($joinDqlParts as $joins) {
            foreach ($joins as $join_) {
                if ($join_->getAlias() === $alias) {
                    return $this;
                }
            }
        }

        $this->queryBuilder->leftJoin($join, $alias, $conditionType, $condition, $indexBy);
        return $this;
    }

    /**
     * @param $join
     * @param $alias
     * @param null $conditionType
     * @param null $condition
     * @param null $indexBy
     * @return $this
     */
    public function addSelect($join, $alias, $conditionType = null, $condition = null, $indexBy = null): self
    {
        $this->leftJoinSafe($join, $alias, $conditionType, $condition, $indexBy);
        $this->queryBuilder->addSelect($alias);
        return $this;
    }


    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder(): QueryBuilder
    {
        return $this->queryBuilder;
    }

    /**
     * @return string
     */
    private function getParameterName(): string
    {
        $this->parameterQ++;
        return sprintf('param_%s', $this->parameterQ);
    }


}
