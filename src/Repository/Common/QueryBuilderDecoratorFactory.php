<?php


namespace App\Repository\Common;


use Doctrine\ORM\QueryBuilder;

class QueryBuilderDecoratorFactory
{

    public  function make(QueryBuilder $queryBuilder): QueryBuilderDecorator
    {
         return new QueryBuilderDecorator($queryBuilder);
    }

}
