<?php


namespace App\Repository\Common;


use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class Pagination
{

    const DEFAULT_LIMIT = 10;

    private $page;

    private $limit;

    private $q;

    public function __construct(int $page = 1, int $limit = self::DEFAULT_LIMIT)
    {
        if (0 >= $page){
            $page = 1;
        }
        $this->page = $page;
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return mixed
     */
    public function getQ()
    {
        return $this->q;
    }

    /**
     * @return int
     */
    public function getCountPages(): int
    {

        $count = $this->q;
        $limit = $this->limit;
        return ceil($count/$limit);
    }

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function handleQueryBuilder(QueryBuilder $queryBuilder): void
    {
        $paginator = new Paginator($queryBuilder);
        $paginator->setUseOutputWalkers(false);
        $this->q = $paginator->count();
        $queryBuilder
            ->setMaxResults($this->limit)
            ->setFirstResult($this->limit * ($this->page - 1));
    }

}
