<?php

namespace App\Repository;

use App\Entity\Product;
use App\Repository\Common\Pagination;
use App\Repository\Common\QueryBuilderDecoratorFactory;
use App\Repository\FilterData\ProductFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    /**
     * @var QueryBuilderDecoratorFactory
     */
    private $queryBuilderDecoratorFactory;

    public function __construct(ManagerRegistry $registry, QueryBuilderDecoratorFactory $queryBuilderDecoratorFactory)
    {
        parent::__construct($registry, Product::class);
        $this->queryBuilderDecoratorFactory = $queryBuilderDecoratorFactory;
    }

    /**
     * @param Product $product
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Product $product): void
    {
       $em = $this->getEntityManager();
       if (!$em->contains($product)){
           $em->persist($product);
       }
       $em->flush($product);
    }

    /**
     * @param Product $product
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Product $product): void
    {
        $em = $this->getEntityManager();
        if ($em->contains($product)){
            $em->remove($product);
            $em->flush($product);
        }
    }

    /**
     * @param string $olx
     * @return Product|null
     */
    public function findByOlxLink(string $olx): ?Product
    {
        return $this->findOneBy(['olxLink' => $olx]);
    }

    /**
     * @param Pagination $pagination
     * @param ProductFilter $filter
     * @return Product[]
     */
    public function getList(Pagination $pagination, ProductFilter $filter): iterable
    {
        $qb = $this->createQueryBuilder('p');
        $qbDecorator = $this->queryBuilderDecoratorFactory->make($qb);

        if (null !== $filter) {
            if (null !== $filter->getManager()) {
                $qbDecorator->equal('p.manager', $filter->getManager());
            }
            if (null !== $filter->getSortById()){
                $qb->addOrderBy('p.id', $filter->getSortById());
            }
        }
        if (null !== $pagination) {
            $pagination->handleQueryBuilder($qb);
        }
        return $qb->getQuery()->getResult();
    }


}
