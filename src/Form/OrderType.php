<?php

namespace App\Form;

use App\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service_locator;

class OrderType extends AbstractType
{

    const GROUP_MAIN = 'GROUP_MAIN';

    const GROUP_ADDRESS = 'GROUP_ADDRESS';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if ($this->isGroup(self::GROUP_MAIN, $options)) {
            $builder
                ->add('firstName')
                ->add('lastName')
                ->add('addName')
                ->add('phone')
                ->add('email', EmailType::class, ['required' => false]);
        }

        if ($this->isGroup(self::GROUP_ADDRESS, $options)) {
            $builder
                ->add('postOfficeRef', HiddenType::class);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            'groups' => [self::GROUP_ADDRESS, self::GROUP_MAIN],
            'csrf_protection' => false
        ]);
    }

    private function isGroup(string $group, array $options): bool
    {
        return in_array($group, $options['groups']);
    }
}
