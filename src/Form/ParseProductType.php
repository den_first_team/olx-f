<?php


namespace App\Form;


use App\Validator\Constraints\OlxOfferLinkUniq;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

class ParseProductType extends AbstractType implements DataTransformerInterface
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('url', TextareaType::class, [
            'constraints' => [new NotBlank(), new Url(), new OlxOfferLinkUniq()]
        ]);

        $builder->get('url')->addViewTransformer($this);

    }

    /**
     * @inheritDoc
     */
    public function transform($value)
    {
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function reverseTransform($value)
    {
        if (is_string($value)){
            return explode('?', $value)[0];
        }
        return $value;
    }
}
