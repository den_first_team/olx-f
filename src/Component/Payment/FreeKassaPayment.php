<?php


namespace App\Component\Payment;


class FreeKassaPayment implements PaymentInterface
{

    /**
     * @var string
     */
    private $merchantID;
    /**
     * @var string
     */
    private $paymentSystemID;
    /**
     * @var string
     */
    private $secretWord;
    /**
     * @var string
     */
    private $url;

    public function __construct(string $merchantID, string $paymentSystemID, string $secretWord, string $url)
    {
        $this->merchantID = $merchantID;
        $this->paymentSystemID = $paymentSystemID;
        $this->secretWord = $secretWord;
        $this->url = $url;
    }

    /**
     * @inheritDoc
     */
    public function createUrl(PaymentData $paymentData): string
    {
        $s = md5(sprintf('%s:%s:%s:%s', $this->merchantID, $paymentData->getAmount(), $this->secretWord, $paymentData->getOrderID()));
        $params = [
            'm' => $this->merchantID,
            'oa' => $paymentData->getAmount(),
            'o' => $paymentData->getOrderID(),
            's' => $s,
            'i' => $this->paymentSystemID
        ];
        return sprintf('%s?%s', $this->url, http_build_query($params));
    }
}
