<?php


namespace App\Component\Payment;


class CurrencyConverterPrivatBank implements CurrencyConverterInterface
{

    const URL_DEFAULT = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $currencies;

    public function __construct(string $url = self::URL_DEFAULT)
    {
        $this->url = $url;
    }

    /**
     * @inheritDoc
     */
    public function uahToUsd(float $amount): float
    {
        $currencies = $this->getCurrencies();
        $rate = 1;
        foreach ($currencies as $curr){
            if ($curr['ccy'] === 'USD'){
                $rate = $curr['buy'];
            }
        }
        $res = $amount/$rate;
        return round($res, 2);
    }

    /**
     * @return array
     */
    private function getCurrencies(): array
    {
        if (null === $this->currencies){
            $this->currencies = json_decode(file_get_contents($this->url),true);
        }
        return $this->currencies;
    }
}
