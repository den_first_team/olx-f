<?php


namespace App\Component\Payment;


interface CurrencyConverterInterface
{

    /**
     * @param float $amount
     * @return float
     */
    public function uahToUsd(float $amount): float;

}
