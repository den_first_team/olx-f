<?php


namespace App\Component\Payment;


interface PaymentInterface
{


    /**
     * @param PaymentData $paymentData
     * @return string
     */
    public function createUrl(PaymentData $paymentData): string;

}
