<?php


namespace App\Component\Payment;


class PaymentData
{

    const CURRENCY_USD = 'usd';

    const CURRENCY_UAH = 'uah';

    /**
     * @var float
     */
    private $amount;

    /**
     * @var string
     */
    private $orderID;

    /**
     * @var string
     */
    private $currency = self::CURRENCY_USD;

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return PaymentData
     */
    public function setAmount(float $amount): PaymentData
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderID(): string
    {
        return $this->orderID;
    }

    /**
     * @param string $orderID
     * @return PaymentData
     */
    public function setOrderID(string $orderID): PaymentData
    {
        $this->orderID = $orderID;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return PaymentData
     */
    public function setCurrency(string $currency): PaymentData
    {
        $this->currency = $currency;
        return $this;
    }


}
