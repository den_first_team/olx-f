<?php


namespace App\Component\Order;


use App\Entity\Order;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class OrderWorkFlow implements OrderWorkFlowInterface
{

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(RequestStack $requestStack, ProductRepository $productRepository)
    {
        $this->requestStack = $requestStack;
        $this->productRepository = $productRepository;
    }

    /**
     * @inheritDoc
     */
    public function saveState(Order $order, string $stateName): void
    {
        $this->getSession()->set('order_state', [$stateName => $order]);
        $this->getSession()->save();
    }

    /**
     * @inheritDoc
     */
    public function getCurrentState(string $stateName): ?Order
    {
        $sessionData = $this->getSession()->get('order_state', []);
        $order = $sessionData[$stateName] ?? null;
        if ($order && $order instanceof Order){
            $product = $order->getProduct();
            $realProduct = $product ? $this->productRepository->find($product->getId()) : null;
            $order->setProduct($realProduct);
            return $order;
        }
        return null;
    }

    /**
     * @return SessionInterface
     */
    private function getSession(): SessionInterface
    {
        return $this->requestStack->getCurrentRequest()->getSession();
    }


    public function clear(): void
    {
        $this->getSession()->remove('order_state');
        $this->getSession()->save();
    }
}
