<?php


namespace App\Component\Order;


use App\Entity\Order;
use App\Entity\OrderPayment;
use App\Repository\OrderPaymentRepository;

class OrderPay
{

    /**
     * @var OrderPaymentRepository
     */
    private $orderPaymentRepository;

    public function __construct(OrderPaymentRepository $orderPaymentRepository)
    {
        $this->orderPaymentRepository = $orderPaymentRepository;
    }

    /**
     * @param Order $order
     * @param float $amount
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function pay(Order $order, float $amount): void
    {
        $payment = (new OrderPayment())->setAmount($amount)->setOrder($order);
        $this->orderPaymentRepository->save($payment);
    }

}
