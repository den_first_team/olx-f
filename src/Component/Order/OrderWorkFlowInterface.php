<?php


namespace App\Component\Order;


use App\Entity\Order;

interface OrderWorkFlowInterface
{

    /**
     * @param Order $order
     * @param string $stateName
     */
    public function saveState(Order $order, string $stateName): void;

    /**
     * @param string $stateName
     * @return Order|null
     */
    public function getCurrentState(string $stateName): ?Order;

    public function clear(): void;

}
