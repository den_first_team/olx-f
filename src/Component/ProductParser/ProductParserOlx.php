<?php


namespace App\Component\ProductParser;

use Symfony\Component\DomCrawler\Crawler;

class ProductParserOlx implements ProductParserInterface
{

    /**
     * @var OlxCurl
     */
    private $olxCurl;

    public function __construct(OlxCurl $olxCurl)
    {
        $this->olxCurl = $olxCurl;
    }

    /**
     * @inheritDoc
     */
    public function parse(string $url): ?ProductInfo
    {
        $html = $this->olxCurl->curl($url)->getBody();
        if (!is_string($html)) {
            return null;
        }

        $product = (new ProductInfo())->setOlxLink($url);
        $crawler = new Crawler($html);

        $nodeName = $crawler->filter('h1');
        $product->setName($nodeName->count() ? $nodeName->first()->text() : null);

        $nodeImage = $crawler->filter('ul#bigGallery > li > a');
        $product->setImg($nodeImage->count() ? $nodeImage->first()->attr('href') : null);

        $images = $crawler->filter('ul#bigGallery > li > a')->each(function (Crawler $crawler, $i) {
            return $crawler->attr('href');
        });
        $product->setImages($images);

        $nodeAddress = $crawler->filter('.offer-titlebox__details > a.show-map-link > strong');
        $product->setAddress($nodeAddress->count() ? $nodeAddress->first()->text() : null);

        $nodeDevice = $crawler->filter('.offer-titlebox__details > em > a');
        $product->setDevice($nodeDevice->count() ? $nodeDevice->first()->text() : null);

        $nodeNumber = $crawler->filter('.offer-titlebox__details > em > small');
        $nodeNumber = $nodeNumber->count() ? str_replace('Номер объявления: ', '', $nodeNumber->first()->text('')) : null;
        $product->setNumber($nodeNumber ?? null);

        $nodeAllDetails = $crawler->filter('.offer-titlebox__details > em');
        if ($nodeAllDetails->count()) {
            $created = str_replace([$product->getDevice(), $product->getNumber(), ', Номер объявления'], '', $nodeAllDetails->first()->text());

            if (is_string($created)) {
                $created = $created ? trim($created) : null;
                $created = substr($created, 0, -1);
                $product->setOfferCreatedAt($created);
            }
        }

        $nodeParamsTable = $crawler->filter('#offerdescription > div > table');
        if ($nodeParamsTable->count()) {
             $nodeParamsTable->first()->filter('table.item > tr')->each(function (Crawler $crawler, $i) use ($product) {
                $fieldNode = $crawler->filter('th');
                $field = $crawler->count() ? $fieldNode->text() : null;
                $valueNode = $crawler->filter('td');
                $value = $valueNode->count() ? $valueNode->text() : null;
                $product->addOption($field, $value);
            });
        }

        $nodeDescription = $crawler->filter('#textContent');
        $product->setDescription($nodeDescription->count() ? $nodeDescription->html() : null);

        $nodePrice = $crawler->filter('#offeractions > div.price-label > strong');
        $priceText = $nodePrice->count() ? preg_replace('/[^0-9]/','',$nodePrice->first()->text('')) : null;
        if (is_numeric($priceText)){
            $product->setPrice(floatval($priceText));
        }

        $authorNode = $crawler->filter('div.offer-user__details h4 a');
        if ($authorNode->count()){
            $product
                ->setAuthor($authorNode->first()->text())
                ->setAuthorLink($authorNode->first()->attr('href'));
        }
        $authorAuthNode = $crawler->filter('div.offer-user__details span.user-since');
        $product->setAuthorRegDate($authorAuthNode->count() ? $authorAuthNode->first()->text() : null);

        $authorOffersLinkNode = $crawler->filter('div.offer-user__details a.user-offers');
        $product->setAuthorOffersLink($authorOffersLinkNode->count() ? $authorOffersLinkNode->first()->attr('href') : null);
    //    die('erfe4rferferferferferferferferf');
        return $product;
    }


}
