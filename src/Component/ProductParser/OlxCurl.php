<?php


namespace App\Component\ProductParser;


class OlxCurl
{

    /**
     * @var string
     */
    private $cookieFilePath;

    public function __construct(string $cookiePath)
    {
        $this->cookieFilePath = $cookiePath;
    }

    /**
     * @param string $url
     * @return OlxCurlResult
     */
    public function curl(string $url): OlxCurlResult
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'upgrade-insecure-requests: 1',
            'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36'
        ]);

        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFilePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFilePath);

        $output = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        curl_close($ch);
        return (new OlxCurlResult($output, $code))->setCookiesFromFile($this->cookieFilePath);
    }

}
