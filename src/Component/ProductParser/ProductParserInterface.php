<?php


namespace App\Component\ProductParser;


interface ProductParserInterface
{

    /**
     * @param string $url
     * @return ProductInfo|null
     */
    public function parse(string $url): ?ProductInfo;

}
