<?php


namespace App\Component\ProductParser;


class ProductOption
{

    /**
     * @var string|null
     */
    private $field;
    /**
     * @var string|null
     */
    private $value;

    public function __construct(?string $field = null, ?string $value = null)
    {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * @return string|null
     */
    public function getField(): ?string
    {
        return $this->field;
    }

    /**
     * @param string|null $field
     * @return ProductOption
     */
    public function setField(?string $field): ProductOption
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return ProductOption
     */
    public function setValue(?string $value): ProductOption
    {
        $this->value = $value;
        return $this;
    }


}
