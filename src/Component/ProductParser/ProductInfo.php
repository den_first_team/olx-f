<?php


namespace App\Component\ProductParser;


class ProductInfo
{

    /**
     * @var string|null
     */
    private $olxLink;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $img;

    /**
     * @var array
     */
    private $images = [];

    /**
     * @var ProductOption[]
     */
    private $options = [];


    /**
     * @var string|null
     */
    private $device;

    /**
     * @var string|null
     */
    private $offerCreatedAt;

    /**
     * @var string|null
     */
    private $number;

    /**
     * @var string|null
     */
    private $address;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var float|null
     */
    private $price;

    /**
     * @var string|null
     */
    private $author;

    /**
     * @var string|null
     */
    private $authorRegDate;

    /**
     * @var string|null
     */
    private $authorOffersLink;

    /**
     * @var string|null
     */
    private $authorLink;

    /**
     * @return string|null
     */
    public function getOlxLink(): ?string
    {
        return $this->olxLink;
    }

    /**
     * @param string|null $olxLink
     * @return ProductInfo
     */
    public function setOlxLink(?string $olxLink): ProductInfo
    {
        $this->olxLink = $olxLink;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return ProductInfo
     */
    public function setName(?string $name): ProductInfo
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImg(): ?string
    {
        return $this->img;
    }

    /**
     * @param string|null $img
     * @return ProductInfo
     */
    public function setImg(?string $img): ProductInfo
    {
        $this->img = $img;
        return $this;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param array $images
     * @return ProductInfo
     */
    public function setImages(array $images): ProductInfo
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return ProductOption[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param string|null $fieldName
     * @param string|null $value
     * @return ProductInfo
     */
    public function addOption(?string $fieldName, ?string $value): ProductInfo
    {
        $this->options[] = new ProductOption($fieldName, $value);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDevice(): ?string
    {
        return $this->device;
    }

    /**
     * @param string|null $device
     * @return ProductInfo
     */
    public function setDevice(?string $device): ProductInfo
    {
        $this->device = $device;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOfferCreatedAt(): ?string
    {
        return $this->offerCreatedAt;
    }

    /**
     * @param string|null $offerCreatedAt
     * @return ProductInfo
     */
    public function setOfferCreatedAt(?string $offerCreatedAt): ProductInfo
    {
        $this->offerCreatedAt = $offerCreatedAt;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string|null $number
     * @return ProductInfo
     */
    public function setNumber(?string $number): ProductInfo
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return ProductInfo
     */
    public function setAddress(?string $address): ProductInfo
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return ProductInfo
     */
    public function setDescription(?string $description): ProductInfo
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     * @return ProductInfo
     */
    public function setPrice(?float $price): ProductInfo
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthor(): ?string
    {
        return $this->author;
    }

    /**
     * @param string|null $author
     * @return ProductInfo
     */
    public function setAuthor(?string $author): ProductInfo
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthorRegDate(): ?string
    {
        return $this->authorRegDate;
    }

    /**
     * @param string|null $authorRegDate
     * @return ProductInfo
     */
    public function setAuthorRegDate(?string $authorRegDate): ProductInfo
    {
        $this->authorRegDate = $authorRegDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthorOffersLink(): ?string
    {
        return $this->authorOffersLink;
    }

    /**
     * @param string|null $authorOffersLink
     * @return ProductInfo
     */
    public function setAuthorOffersLink(?string $authorOffersLink): ProductInfo
    {
        $this->authorOffersLink = $authorOffersLink;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthorLink(): ?string
    {
        return $this->authorLink;
    }

    /**
     * @param string|null $authorLink
     * @return ProductInfo
     */
    public function setAuthorLink(?string $authorLink): ProductInfo
    {
        $this->authorLink = $authorLink;
        return $this;
    }

}
