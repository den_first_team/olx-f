<?php


namespace App\Component\ProductParser;


use App\Entity\Product;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;

class ProductParserEntityCreator
{

    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;
    /**
     * @var PropertyInfoExtractor
     */
    private $propertyInfo;
    /**
     * @var ProductParserInterface
     */
    private $productParser;

    public function __construct(PropertyAccessor $propertyAccessor, PropertyInfoExtractor $propertyInfo, ProductParserInterface $productParser)
    {

       // $this->propertyAccessor = $propertyAccessor;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        $this->propertyInfo = $propertyInfo;
        $this->productParser = $productParser;
    }

    /**
     * @param ProductInfo $productInfo
     * @param Product $product
     */
    public function load(ProductInfo $productInfo, Product $product): void
    {
        $exclude = ['options', 'authorLink'];
        foreach ($this->propertyInfo->getProperties(get_class($productInfo)) as $attr){
            if (in_array($attr, $exclude)){
                continue;
            }
            $value = $this->propertyAccessor->getValue($productInfo, $attr);
            $this->propertyAccessor->setValue($product, $attr, $value);
        }
        if ($productInfo->getOptions()){
            foreach ($productInfo->getOptions() as $option){
                if (null === $option->getField()){
                    continue;
                }
                $product->addOption($option->getField(), $option->getValue());
            }
        }
    }

    /**
     * @param ProductInfo $productInfo
     * @return Product
     */
    public function create(ProductInfo $productInfo): Product
    {
        $product = new Product();
        $this->load($productInfo, $product);
        return $product;
    }

    /**
     * @param string $url
     * @return Product
     */
    public function createByUrl(string $url): Product
    {
        $productInfo = $this->productParser->parse($url);
        return $this->create($productInfo);
    }

    /**
     * @return ProductParserInterface
     */
    public function getProductParser(): ProductParserInterface
    {
        return $this->productParser;
    }

}
