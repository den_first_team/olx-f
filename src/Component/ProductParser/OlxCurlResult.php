<?php


namespace App\Component\ProductParser;


class OlxCurlResult
{


    /**
     * @var string|null
     */
    private $body;
    /**
     * @var int|null
     */
    private $code;
    /**
     * @var array
     */
    private $headers;

    /**
     * @var string|null
     */
    private $cookies;

    public function __construct(?string $body, ?int $code, $headers = [])
    {

        $this->body = $body;
        $this->code = $code;
        $this->headers = $headers;
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param string|null $body
     * @return OlxCurlResult
     */
    public function setBody(?string $body): OlxCurlResult
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCode(): ?int
    {
        return $this->code;
    }

    /**
     * @param int|null $code
     * @return OlxCurlResult
     */
    public function setCode(?int $code): OlxCurlResult
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     * @return OlxCurlResult
     */
    public function setHeaders(array $headers): OlxCurlResult
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCookies(): ?string
    {
        return $this->cookies;
    }

    /**
     * @param string|null $cookies
     * @return OlxCurlResult
     */
    public function setCookies(?string $cookies): OlxCurlResult
    {
        $this->cookies = $cookies;
        return $this;
    }

    public function setCookiesFromFile(string  $filePath): self
    {
        if (is_file($filePath)){
            $this->cookies = file_get_contents($filePath);
        }
        return $this;
    }

}
