<?php


namespace App\Component\Product;


use App\Entity\Product;

interface ProductAliasGeneratorInterface
{

    public function generate(Product $product): ?string;

}
