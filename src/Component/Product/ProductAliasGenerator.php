<?php


namespace App\Component\Product;


use App\Entity\Product;

class ProductAliasGenerator implements ProductAliasGeneratorInterface
{

    public function generate(Product $product): ?string
    {
        if (null === $product->getOlxLink()){
            return null;
        }
        $exp = explode('/', $product->getOlxLink());
        return array_slice($exp,-1, 1)[0];
    }
}
