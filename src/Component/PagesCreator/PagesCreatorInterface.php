<?php


namespace App\Component\PagesCreator;


interface PagesCreatorInterface
{

    /**
     * @param int $itemsInPage
     * @param int $itemsCount
     * @param int $currentPage
     * @return PagesInterface
     */
    public function create(int $itemsInPage, int $itemsCount, int $currentPage = 1): PagesInterface;

}
