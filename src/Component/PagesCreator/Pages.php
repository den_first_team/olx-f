<?php


namespace App\Component\PagesCreator;


class Pages implements PagesInterface
{

    /**
     * @var PageIterator
     */
    private $all;
    /**
     * @var int
     */
    private $itemsInPage;
    /**
     * @var int
     */
    private $itemsCount;
    /**
     * @var int
     */
    private $currentPage;

    public function __construct(int $itemsInPage, int $itemsCount, int $currentPage = 1)
    {
        $this->itemsInPage = $itemsInPage;
        $this->itemsCount = $itemsCount;
        $this->currentPage = $currentPage;
    }

    /**
     * @return PageIterator
     */
    public function all(): PageIterator
    {
        if (null === $this->all) {
            $pages = $this->itemsCount / $this->itemsInPage;
            $pages = ceil($pages);
            $this->all = new PageIterator();
            for ($i = 1; $i <= $pages; $i++){
                $this->all->add($i, $i === $this->currentPage);
            }
        }
        return $this->all;
    }

    /**
     * @return bool
     */
    public function needDisplay(): bool
    {
        return iterator_count($this->all()) > 1;
    }

    /**
     * @inheritDoc
     */
    public function isValidCurrentPage(): bool
    {
        if (1 === $this->currentPage){
            return true;
        }
        return $this->currentPage > 0 && $this->currentPage <= $this->countPages();
    }

    /**
     * @return int
     */
    private function countPages(): int
    {
        return iterator_count($this->all());
    }
}
