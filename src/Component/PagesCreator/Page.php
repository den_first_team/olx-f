<?php


namespace App\Component\PagesCreator;


class Page implements PageInterface
{

    /**
     * @var int
     */
    private $number;
    /**
     * @var bool
     */
    private $active;

    public function __construct(int $page, bool $active = false)
    {
        $this->number = $page;
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return Page
     */
    public function setNumber(int $number): Page
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Page
     */
    public function setActive(bool $active): Page
    {
        $this->active = $active;
        return $this;
    }
}
