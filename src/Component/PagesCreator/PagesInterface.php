<?php


namespace App\Component\PagesCreator;


interface PagesInterface
{

    /**
     * @return PageIterator
     */
    public function all(): PageIterator;

    /**
     * @return bool
     */
    public function needDisplay(): bool;

    /**
     * @return bool
     */
    public function isValidCurrentPage(): bool;

}
