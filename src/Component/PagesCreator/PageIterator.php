<?php


namespace App\Component\PagesCreator;


class PageIterator  implements \Iterator
{

    protected $elements = [];

    protected $position;

    public function __construct(array $array = [])
    {
        $this->position = 0;
        $this->elements = $array;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current(): PageInterface
    {
        return $this->elements[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid(): bool
    {
        return isset($this->elements[$this->position]);
    }

    /**
     * @param PageInterface $page
     */
    public function addElement(PageInterface $page): void
    {
        $this->elements[] = $page;
    }

    public function add(int $page, $active = false): void
    {
        $this->addElement(new Page($page, $active));
    }
}
