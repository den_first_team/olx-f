<?php


namespace App\Component\PagesCreator;


interface PageInterface
{

    /**
     * @return int
     */
    public function getNumber(): int;

    /**
     * @return bool
     */
    public function isActive(): bool;

}
