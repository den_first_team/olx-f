<?php


namespace App\Component\PagesCreator;


use App\Repository\Common\Pagination;

class DoctrinePagesCreator
{

    /**
     * @var PagesCreatorInterface
     */
    private $pagesCreator;

    /**
     * DoctrinePagesCreator constructor.
     * @param PagesCreatorInterface $pagesCreator
     */
    public function __construct(PagesCreatorInterface $pagesCreator)
    {
        $this->pagesCreator = $pagesCreator;
    }

    /**
     * @param Pagination $pagination
     * @return PagesInterface
     */
    public function create(Pagination $pagination): PagesInterface
    {
        return $this->pagesCreator->create($pagination->getLimit(), $pagination->getQ(), $pagination->getPage());
    }

}
