<?php


namespace App\Component\User;


use App\Entity\User;

interface UserPasswordSetInterface
{

    /**
     * @param User $user
     * @param string|null $password
     */
    public function set(User $user, ?string $password): void;

}
