<?php


namespace App\Component\User;


use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserPasswordSet implements UserPasswordSetInterface
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @inheritDoc
     */
    public function set(User $user, ?string $password): void
    {
        if (null === $password){
            $password = $user->getPassword();
        }
        if (null === $password){
            throw new PasswordIsNullException();
        }
        $pass = $this->encoder->encodePassword($user, $password);
        $user->setPassword($pass);
    }
}
