<?php


namespace App\Component\User;


use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AuthUser implements AuthUserInterface
{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @inheritDoc
     */
    public function isAuth(): bool
    {
        return null !== $this->getUser();
    }

    /**
     * @inheritDoc
     */
    public function getUser(): ?User
    {
        $token = $this->tokenStorage->getToken();
        if (null === $token){
            return null;
        }
        $user = $token->getUser();
        if (is_object($user) && $user instanceof User){
            return $user;
        }
        return null;
    }
}
