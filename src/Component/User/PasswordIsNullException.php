<?php


namespace App\Component\User;


class PasswordIsNullException extends \Exception
{

    protected $message = 'Password is null';

}
