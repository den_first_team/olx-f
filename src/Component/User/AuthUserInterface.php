<?php


namespace App\Component\User;


use App\Entity\User;

interface AuthUserInterface
{

    /**
     * @return bool
     */
    public function isAuth(): bool;

    /**
     * @return User|null
     */
    public function getUser(): ?User;

}
