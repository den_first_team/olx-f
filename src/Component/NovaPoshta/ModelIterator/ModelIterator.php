<?php


namespace App\Component\NovaPoshta\ModelIterator;



class ModelIterator implements \Iterator
{

    protected $elements = [];

    protected $position;

    public function __construct(array $array = []) {
        $this->position = 0;
        $this->elements = $array;
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return $this->elements[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->elements[$this->position]);
    }

    /**
     * @param $entity
     * @return mixed
     */
    public function append($entity)
    {
        $this->elements[] = $entity;
    }
}
