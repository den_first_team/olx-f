<?php


namespace App\Component\NovaPoshta\ModelIterator;


use App\Component\NovaPoshta\Model\PostOffice;

class PostOfficeIterator extends ModelIterator
{

    /**
     * @return PostOffice
     */
    public function current(): PostOffice
    {
        return parent::current();
    }

}
