<?php


namespace App\Component\NovaPoshta;


interface RequestTypeInterface
{

    /**
     * @return string
     */
    public function getContentType(): string;

    /**
     * @return string
     */
    public function getBaseUrl(): string;

    /**
     * @param string|null $responseBody
     * @return array|null
     */
    public function normalizeResponseBody(?string $responseBody): ?array;

    /**
     * @param array|null $data
     * @return mixed
     */
    public function normalizeRequestParams(?array $data);

}
