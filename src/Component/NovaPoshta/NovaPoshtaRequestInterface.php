<?php


namespace App\Component\NovaPoshta;


interface NovaPoshtaRequestInterface
{


    public function req(string $resourceName, array $data = []): NovaPoshtaResponse;

}
