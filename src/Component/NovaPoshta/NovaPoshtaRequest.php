<?php


namespace App\Component\NovaPoshta;


class NovaPoshtaRequest implements NovaPoshtaRequestInterface
{

    /**
     * @var string
     */
    private $authKey;
    /**
     * @var RequestTypeInterface
     */
    private $requestType;

    public function __construct(string $authKey, RequestTypeInterface $requestType)
    {
        $this->authKey = $authKey;
        $this->requestType = $requestType;
    }

    public function req(string $resourceName, array $data = []): NovaPoshtaResponse
    {
        [$model, $method] = explode(':', $resourceName);
        $data_ = [
            'apiKey' => $this->authKey,
            'modelName' => $model,
            'calledMethod' => $method,
            'methodProperties' => $data
        ];

        $headers['Content-Type'] = $this->requestType->getContentType();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->requestType->getBaseUrl());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestType->normalizeRequestParams($data_));
        $output = curl_exec($ch);
        curl_close($ch);
        return new NovaPoshtaResponse($this->requestType->normalizeResponseBody($output));
    }
}
