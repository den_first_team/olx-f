<?php


namespace App\Component\NovaPoshta\Repository;


use App\Component\NovaPoshta\Model\PostOffice;
use App\Component\NovaPoshta\ModelIterator\PostOfficeIterator;
use App\Component\NovaPoshta\NovaPoshtaRequestInterface;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
class PostOfficeRepository
{

    public const DEFAULT_LIMIT = 500;

    /**
     * @var NovaPoshtaRequestInterface
     */
    private $novaPoshtaRequest;
    /**
     * @var ArrayDenormalizer
     */
    private $arrayDenormalizer;

    public function __construct(NovaPoshtaRequestInterface $novaPoshtaRequest, ArrayDenormalizer $arrayDenormalizer)
    {
        $this->novaPoshtaRequest = $novaPoshtaRequest;
        $this->arrayDenormalizer = $arrayDenormalizer;
    }

    /**
     * @param string $cityRef
     * @param int $limit
     * @return array
     */
    public function getAsArray(string $cityRef, $limit = self::DEFAULT_LIMIT): array
    {

        $data =  $this->novaPoshtaRequest
            ->req('Address:getWarehouses', [
                'Limit' => $limit,
                'CityRef' => $cityRef
            ])->getData();

        return $data ?? [];
    }

    /**
     * @param string $cityRef
     * @param int $limit
     * @return PostOfficeIterator
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function get(string $cityRef, $limit = self::DEFAULT_LIMIT): PostOfficeIterator
    {
        $dataArr = $this->getAsArray($cityRef, $limit);
        $data = $this->arrayDenormalizer->denormalize($dataArr, PostOffice::class.'[]');
        return new PostOfficeIterator($data);
    }

}
