<?php


namespace App\Component\NovaPoshta\Repository;


use App\Component\NovaPoshta\Model\City;
use App\Component\NovaPoshta\ModelIterator\CityIterator;
use App\Component\NovaPoshta\NovaPoshtaRequestInterface;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class CityRepository
{

    public const DEFAULT_LIMIT = 30;

    /**
     * @var NovaPoshtaRequestInterface
     */
    private $novaPoshtaRequest;

    /**
     * @var ArrayDenormalizer
     */
    private $objectNormalizer;

    public function __construct(NovaPoshtaRequestInterface $novaPoshtaRequest, ArrayDenormalizer $objectNormalizer)
    {
        $this->novaPoshtaRequest = $novaPoshtaRequest;
        $this->objectNormalizer = $objectNormalizer;
    }


    /**
     * @param string $name
     * @param int $limit
     * @return array
     */
    public function getAsArray(string $name, $limit = self::DEFAULT_LIMIT): array
    {
        $data =  $this->novaPoshtaRequest
            ->req('Address:searchSettlements', [
                'Limit' => $limit,
                'CityName' => $name
            ])->getData();

        return $data[0]['Addresses'] ?? [];
    }

    /**
     * @param string $name
     * @param int $limit
     * @return CityIterator
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function get(string $name, $limit = self::DEFAULT_LIMIT): CityIterator
    {
        $dataArr = $this->getAsArray($name, $limit);
        $data = $this->objectNormalizer->denormalize($dataArr, City::class.'[]');
        return new CityIterator($data);
    }

}
