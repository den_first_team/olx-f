<?php


namespace App\Component\NovaPoshta\Model;


use Symfony\Component\Serializer\Annotation\SerializedName;

class City
{

    /**
     * @SerializedName("MainDescription")
     * @var string|null
     */
    private $name;

    /**
     * @SerializedName("Present")
     * @var|null
     */
    private $fullName;

    /**
     * @SerializedName("Ref")
     * @var string|null
     */
    private $ref;

    /**
     * @SerializedName("DeliveryCity")
     * @var string|null
     */
    private $refAdd;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return City
     */
    public function setName(?string $name): City
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     * @return City
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @param string|null $ref
     * @return City
     */
    public function setRef(?string $ref): City
    {
        $this->ref = $ref;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRefAdd(): ?string
    {
        return $this->refAdd;
    }

    /**
     * @param string|null $refAdd
     * @return City
     */
    public function setRefAdd(?string $refAdd): City
    {
        $this->refAdd = $refAdd;
        return $this;
    }

}
