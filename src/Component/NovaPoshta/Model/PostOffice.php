<?php


namespace App\Component\NovaPoshta\Model;


use Symfony\Component\Serializer\Annotation\SerializedName;

class PostOffice
{

    /**
     * @SerializedName("Description")
     * @var string|null
     */
   private $name;

    /**
     * @SerializedName("Number")
     * @var string|null
     */
   private $number;

    /**
     * @SerializedName("Ref")
     * @var string|null
     */
   private $ref;

    /**
     * @SerializedName("CityRef")
     * @var string|null
     */
   private $cityRef;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return PostOffice
     */
    public function setName(?string $name): PostOffice
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @param string|null $number
     * @return PostOffice
     */
    public function setNumber(?string $number): PostOffice
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @param string|null $ref
     * @return PostOffice
     */
    public function setRef(?string $ref): PostOffice
    {
        $this->ref = $ref;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCityRef(): ?string
    {
        return $this->cityRef;
    }

    /**
     * @param string|null $cityRef
     * @return PostOffice
     */
    public function setCityRef(?string $cityRef): PostOffice
    {
        $this->cityRef = $cityRef;
        return $this;
    }

}
