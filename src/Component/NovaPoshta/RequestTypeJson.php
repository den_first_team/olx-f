<?php


namespace App\Component\NovaPoshta;


class RequestTypeJson implements RequestTypeInterface
{

    /**
     * @inheritDoc
     */
    public function getContentType(): string
    {
        return 'application/json';
    }

    /**
     * @inheritDoc
     */
    public function getBaseUrl(): string
    {
        return 'https://api.novaposhta.ua/v2.0/json/';
    }

    /**
     * @inheritDoc
     */
    public function normalizeResponseBody(?string $responseBody): ?array
    {
        return json_decode($responseBody, true);
    }

    /**
     * @inheritDoc
     */
    public function normalizeRequestParams(?array $data)
    {
        return json_encode($data);
    }
}
