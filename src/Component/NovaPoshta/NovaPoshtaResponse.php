<?php


namespace App\Component\NovaPoshta;


class NovaPoshtaResponse
{

    private $data = [];
    /**
     * @var int
     */
    private $status;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var array|null
     */
    private $originData;

    public function __construct(?array $respData = null, int $status = 200)
    {
        $this->status = $status;
        $this->originData = $respData;

        if (null !== $respData){
            if (isset($respData['data'])){
                $this->data = $respData['data'];
            }

            if (isset($respData['errors']) && is_array($respData['errors'])){
                $this->errors = $respData['errors'];
            }

            if (isset($respData['success'])){
                $this->status = $respData['success'] === true ? 200 : 409;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return array|null
     */
    public function getOriginData(): ?array
    {
        return $this->originData;
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed|null
     */
    public function getDataByKey(string $key, $default = null)
    {
        return $this->data[$key] ?? $default;
    }

}
