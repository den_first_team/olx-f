<?php


namespace App\Security\Voter;


use App\Entity\Product;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ProductEditAccessVoter extends Voter
{

    const ATTRIBUTE = 'app.ProductEditAccessVoter';

    /**
     * @inheritDoc
     */
    protected function supports(string $attribute, $subject)
    {
        return self::ATTRIBUTE === $attribute && $subject instanceof Product;
    }

    /**
     * @param string $attribute
     * @param Product $subject
     * @param TokenInterface $token
     * @return bool|void
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        return $token->getUser() === $subject->getManager();
    }
}
