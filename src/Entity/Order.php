<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="order_product")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $productId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cityRef;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $postOfficeRef;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     */
    private $product;

    /**
     * @ORM\OneToMany(targetEntity="OrderPayment", mappedBy="order")
     */
    private $payments;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $addName;

    public function __construct()
    {
        $this->payments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getProductId(): ?int
    {
        return $this->productId;
    }

    public function setProductId(?int $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    public function getCityRef(): ?string
    {
        return $this->cityRef;
    }

    public function setCityRef(?string $cityRef): self
    {
        $this->cityRef = $cityRef;

        return $this;
    }

    public function getPostOfficeRef(): ?string
    {
        return $this->postOfficeRef;
    }

    public function setPostOfficeRef(?string $postOfficeRef): self
    {
        $this->postOfficeRef = $postOfficeRef;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return Collection|OrderPayment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(OrderPayment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setOrder($this);
        }

        return $this;
    }

    public function removePayment(OrderPayment $payment): self
    {
        if ($this->payments->contains($payment)) {
            $this->payments->removeElement($payment);
            // set the owning side to null (unless already changed)
            if ($payment->getOrder() === $this) {
                $payment->setOrder(null);
            }
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->getProductAmount() + $this->getDeliveryAmount();
    }

    /**
     * @return float
     */
    public function getProductAmount(): float
    {
        return $this->product ? (float)$this->getProduct()->getPrice() : 0;
    }

    /**
     * @return float
     */
    public function getDeliveryAmount(): float
    {
        return 50;
    }

    /**
     * @return float
     */
    public function getPaymentAmount(): float
    {
        $result = 0;
        foreach ($this->getPayments() as $payment){
            $result += $payment->getAmount();
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        if (null === $this->getProduct()){
            return false;
        }
        return $this->getPaymentAmount() >= $this->getAmount();
    }

    public function getAddName(): ?string
    {
        return $this->addName;
    }

    public function setAddName(?string $addName): self
    {
        $this->addName = $addName;

        return $this;
    }

}
