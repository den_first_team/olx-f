<?php


namespace App\Controller;

use App\Entity\Product;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/products")
 * Class ProductController
 * @package App\Controller
 */
class ProductController extends DefaultController
{

    /**
     * @Route("/{alias}", methods={"GET"}, name="products")
     * @param Product $product
     * @return Response
     */
    public function view(Product $product): Response
    {
        return $this->render('products.html.twig', ['pr' => $product]);
    }

}
