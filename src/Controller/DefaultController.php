<?php


namespace App\Controller;


use App\Component\PagesCreator\DoctrinePagesCreator;
use App\Component\PagesCreator\PagesInterface;
use App\Repository\Common\Pagination;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends AbstractController
{
    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(),[
            'app.doctrine_pages_creator' => '?'.DoctrinePagesCreator::class
        ]);
    }

    private function createFormByName(string $type, $data = null, array $options = [], $name = ''): FormInterface
    {
        return $this->get('form.factory')->createNamed($name, $type, $data, $options);
    }

    /**
     * @param Request $request
     * @param string $type
     * @param null $data
     * @param array $options
     * @return FormInterface
     */
    protected function processForm(Request $request, string $type, $data = null, array $options = []): FormInterface
    {
        $form = $this->createForm($type, $data, $options);
        $form->handleRequest($request);
        return $form;
    }

    /**
     * @param int $page
     * @param int $limit
     * @return Pagination
     */
    protected function createPagination($page = 1, $limit = Pagination::DEFAULT_LIMIT): Pagination
    {
        return new Pagination($page, $limit);
    }

    /**
     * @param Pagination $pagination
     * @return PagesInterface
     */
    protected function createPages(Pagination $pagination): PagesInterface
    {
        return $this->get('app.doctrine_pages_creator')->create($pagination);
    }

}
