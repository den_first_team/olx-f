<?php


namespace App\Controller\Admin;


use App\Component\ProductParser\ProductParserEntityCreator;
use App\Component\User\AuthUserInterface;
use App\Controller\DefaultController;
use App\Entity\Product;
use App\Form\ParseProductType;
use App\Form\ProductEditType;
use App\Repository\FilterData\ProductFilter;
use App\Repository\ProductRepository;
use App\Security\Voter\ProductEditAccessVoter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/products")
 * Class ProductController
 * @package App\Controller\Admin
 */
class ProductController extends DefaultController
{

    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var AuthUserInterface
     */
    private $authUser;
    /**
     * @var ProductParserEntityCreator
     */
    private $productParserEntityCreator;

    public function __construct(ProductRepository $productRepository, AuthUserInterface $authUser, ProductParserEntityCreator $productParserEntityCreator)
    {
        $this->productRepository = $productRepository;
        $this->authUser = $authUser;
        $this->productParserEntityCreator = $productParserEntityCreator;
    }

    /**
     * @Route("/{page}", methods={"POST", "GET"}, name="admin.products")
     * @param Request $request
     * @param int $page
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function index(Request $request, int $page = 1): Response
    {

        $form = $this->processForm($request, ParseProductType::class);
        $productNew = null;
       // dd($form->get('url')->getData()); die();
        if ($form->isSubmitted() && $form->isValid()){
            $productNew = $this->productParserEntityCreator->createByUrl($form->get('url')->getData());
            $this->productRepository->save($productNew);
        }

        $filter = new ProductFilter();
        $filter->setManager($this->authUser->getUser())->setSortById('DESC');

        $pagination = $this->createPagination($page);
        $products = $this->productRepository->getList($pagination, $filter);
        $pages = $this->createPages($pagination);

        if (false === $pages->isValidCurrentPage()){
            throw $this->createNotFoundException();
        }

        return $this->render('admin/products.html.twig', [
            'form' => $form->createView(),
            'product_new' => $productNew,
            'products' => $products,
            'pages' => $pages
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin.products.delete")
     * @param Product $product
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Product $product): Response
    {
        $this->denyAccessUnlessGranted(ProductEditAccessVoter::ATTRIBUTE, $product);
        $this->productRepository->remove($product);
        return $this->redirectToRoute('admin.products');
    }

    /**
     * @Route("/edit/{id}", methods={"POST", "GET"},  name="admin.products.edit")
     * @param Product $product
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Product $product, Request $request): Response
    {
        $this->denyAccessUnlessGranted(ProductEditAccessVoter::ATTRIBUTE, $product);
        $form = $this->processForm($request, ProductEditType::class, $product);

        if ($form->isSubmitted() && $form->isValid()){
            $this->productRepository->save($product);
            return $this->redirectToRoute('admin.products');
        }
        return $this->render('admin/products_edit.html.twig', ['product' => $product, 'form' => $form->createView()]);
    }

}
