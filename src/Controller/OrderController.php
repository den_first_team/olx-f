<?php


namespace App\Controller;


use App\Component\Order\OrderPay;
use App\Component\Order\OrderWorkFlowInterface;
use App\Entity\Order;
use App\Entity\Product;
use App\Form\OrderType;
use App\Repository\OrderRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/order")
 * Class OrderController
 * @package App\Controller
 */
class OrderController extends DefaultController
{

    const ORDER_STATE_INFO = 'ORDER_STATE_INFO';

    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var OrderWorkFlowInterface
     */
    private $orderWorkFlow;
    /**
     * @var OrderPay
     */
    private $orderPay;

    public function __construct(OrderRepository $orderRepository, OrderWorkFlowInterface $orderWorkFlow, OrderPay $orderPay)
    {
        $this->orderRepository = $orderRepository;
        $this->orderWorkFlow = $orderWorkFlow;
        $this->orderPay = $orderPay;
    }

    /**
     * @Route("/prev-info/{id}", methods={"GET"}, name="order.prev_info")
     * @param Product $product
     * @return Response
     */
    public function prevInfo(Product $product): Response
    {
        return $this->render('order.prev_info.html.twig', ['pr' => $product]);
    }

    /**
     * @Route("/info/{id}", methods={"POST", "GET"}, name="order.info")
     * @param Request $request
     * @param Product $product
     * @return Response
     */
    public function mainInfo(Request $request, Product $product): Response
    {
        $order = $this->orderWorkFlow->getCurrentState(self::ORDER_STATE_INFO);
        if (!$order){
            $order = new Order();
        }
        $order ->setProduct($product);

        $form = $this->processForm($request, OrderType::class, $order, ['validation_groups'=>['Order'], 'groups' => [OrderType::GROUP_MAIN]]);
        if ($form->isSubmitted() && $form->isValid()){
            $this->orderWorkFlow->saveState($order, self::ORDER_STATE_INFO);
            return $this->redirectToRoute('order.address');
        }

        return $this->render('order.info.html.twig', ['form' => $form->createView(), 'pr' => $product]);
    }

    /**
     * @Route("/addresses", methods={"POST", "GET"},  name="order.address")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function address(Request $request): Response
    {

        $order = $this->orderWorkFlow->getCurrentState(self::ORDER_STATE_INFO);
        if (null === $order){
            throw $this->createNotFoundException();
        }

        $form = $this->processForm($request, OrderType::class, $order, ['groups' => [OrderType::GROUP_ADDRESS]]);
        if ($form->isSubmitted() && $form->isValid()){
            $this->orderRepository->save($order);
            //$this->orderWorkFlow->clear();
            return $this->redirectToRoute('order.payment', ['id' => $order->getId()]);
        }
        return $this->render('order.address.html.twig', ['form' => $form->createView(), 'order' => $order]);

    }

    /**
     * @Route("/payment/{id}", methods={"POST", "GET"},  name="order.payment")
     * @param Request $request
     * @param Order $order
     * @return Response
     */
    public function payment(Request $request, Order $order): Response
    {
        $merchantID = $this->getParameter('app.payment.wallet_one.merchant_id');
        return $this->render('order.payment.html.twig', ['order' => $order, 'merchantID'=>$merchantID]);
    }

    /**
     * @Route("/payment-result-success", methods={"POST", "GET"},  name="order.payment_result_success")
     * @param Request $request
     * @return Response
     */
    public function paymentResultSuccess(Request $request): Response
    {
        $order = $this->orderWorkFlow->getCurrentState(self::ORDER_STATE_INFO);
        if (null === $order){
            throw $this->createNotFoundException();
        }
        return $this->render('order.payment_success.html.twig', ['order' => $order]);
    }

    /**
     * @Route("/payment-result-error", methods={"POST", "GET"},  name="order.payment_result_error")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function paymentResultError(Request $request)
    {
        return $this->redirect('https://olx.ua');
        //dd($request->request->all(), $request->query->all());

    }

}
