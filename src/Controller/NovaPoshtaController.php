<?php


namespace App\Controller;

use App\Component\NovaPoshta\Repository\CityRepository;
use App\Component\NovaPoshta\Repository\PostOfficeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/nova-poshta")
 * Class NovaPoshtaController
 * @package App\Controller
 */
class NovaPoshtaController extends DefaultController
{

    /**
     * @Route("/cities", methods={"GET"})
     * @param Request $request
     * @param CityRepository $cityRepository
     * @return JsonResponse
     */
    public function city(Request $request, CityRepository $cityRepository): JsonResponse
    {
        $name = $request->query->get('name');

        if (!is_string($name)){
            throw $this->createNotFoundException();
        }

        $result = $cityRepository->getAsArray($name);
        return new JsonResponse($result);
    }


    /**
     * @Route("/offices", methods={"GET"})
     * @param Request $request
     * @param PostOfficeRepository $postOfficeRepository
     * @return JsonResponse
     */
    public function postOffice(Request $request, PostOfficeRepository $postOfficeRepository): JsonResponse
    {
        $ref = $request->query->get('cityRef');
        if (!is_string($ref)){
            throw $this->createNotFoundException();
        }
        $result = $postOfficeRepository->getAsArray($ref);
        return new JsonResponse($result);
    }

}
