<?php


namespace App;


use App\Component\NovaPoshta\NovaPoshtaRequest;
use App\Component\Payment\FreeKassaPayment;
use App\Component\ProductParser\OlxCurl;
use App\EventListener\Doctrine\DoctrinePostLoadListenerInterface;
use App\EventListener\Doctrine\DoctrinePostSaveListenerInterface;
use App\EventListener\Doctrine\DoctrinePreSaveListenerInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class Services
{

    /**
     * @param ContainerConfigurator $containerConfigurator
     * @param ContainerBuilder $containerBuilder
     */
    public static function configureServices(ContainerConfigurator $containerConfigurator, ContainerBuilder $containerBuilder): void
    {
        $containerBuilder->autowire(FreeKassaPayment::class)
            ->setArguments([
                '$merchantID' => '%app.payment.free_kassa.merchant_id%',
                '$paymentSystemID' => '%app.payment.free_kassa.payment_system_id%',
                '$secretWord' => '%app.payment.free_kassa.secret_word%',
                '$url' => '%app.payment.free_kassa.url%'
            ]);
        $containerBuilder->autowire(PropertyAccessor::class);

        $containerBuilder->autowire(ReflectionExtractor::class);
        $containerBuilder->autowire(PropertyInfoExtractor::class)
            ->addArgument([new Reference(ReflectionExtractor::class)])
            ->addArgument([new Reference(ReflectionExtractor::class)])
        ;

        /**
         * Doctrine
         */
        $containerBuilder
            ->registerForAutoconfiguration(DoctrinePostSaveListenerInterface::class)
            ->addTag('doctrine.event_listener', [
                'event' => Events::postPersist,
                'priority' => 0
            ])
            ->addTag('doctrine.event_listener', [
                'event' => Events::postUpdate,
                'priority' => 0
            ]);
        $containerBuilder
            ->registerForAutoconfiguration(DoctrinePreSaveListenerInterface::class)
            ->addTag('doctrine.event_listener', [
                'event' => Events::prePersist,
                'priority' => 0
            ])
            ->addTag('doctrine.event_listener', [
                'event' => Events::preUpdate,
                'priority' => 0
            ]);
        $containerBuilder
            ->registerForAutoconfiguration(DoctrinePostLoadListenerInterface::class)
            ->addTag('doctrine.event_listener', [
                'event' => Events::postLoad,
                'priority' => 0
            ]);

        $containerBuilder->setAlias(EntityManager::class, EntityManagerInterface::class);
        $containerBuilder->autowire(OlxCurl::class)->setArgument('$cookiePath', '%kernel.project_dir%/var/cookies.txt');

        $containerBuilder->autowire(NovaPoshtaRequest::class)->addArgument('%app.nova_poshta_key%');

        $containerBuilder->setAlias(Serializer::class, SerializerInterface::class);
        $containerBuilder->autowire(ArrayDenormalizer::class)->addMethodCall('setSerializer', [new Reference(SerializerInterface::class)]);
    }

}
