<?php


namespace App\Command;


use App\Component\ProductParser\OlxCurl;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestOlxCommand extends Command
{

    protected static $defaultName = 'app.test-olx';
    /**
     * @var OlxCurl
     */
    private $olxCurl;

    public function __construct(OlxCurl $olxCurl, string $name = null)
    {
        parent::__construct($name);
        $this->olxCurl = $olxCurl;
    }

    protected function configure()
    {
        $this->addArgument('url', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        dd($this->olxCurl->curl($input->getArgument('url')));
    }

}
