<?php


namespace App\Command;


use App\Component\User\UserPasswordSetInterface;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateAdminCommand extends Command
{

    protected static $defaultName = 'app:create-admin';
    /**
     * @var UserPasswordSetInterface
     */
    private $userPasswordSet;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        UserPasswordSetInterface $userPasswordSet,
        UserRepository $userRepository,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->userPasswordSet = $userPasswordSet;
        $this->userRepository = $userRepository;
    }

    protected function configure()
    {
        $this
            ->addArgument('login', InputArgument::REQUIRED)
            ->addArgument('password', InputArgument::OPTIONAL, '', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $login = $input->getArgument('login');
        $password = $input->getArgument('password');
        $user = $this->userRepository->findByLogin($login);

        if (null === $user){
            $user = (new User())->setEmail($login);
            if (!$password){
                throw new \Exception('Password is null');
            }
        }

        if ($password){
            $this->userPasswordSet->set($user, $password);
        }
        $user->addRole(User::ROLE_ADMIN);
        $this->userRepository->save($user);
        $output->writeln('<fg=green>Success!</>');
        return 1;

    }
}
