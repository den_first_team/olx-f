<?php


namespace App\Command;


use App\Component\NovaPoshta\NovaPoshtaRequest;
use App\Component\NovaPoshta\Repository\CityRepository;
use App\Component\NovaPoshta\Repository\PostOfficeRepository;
use App\Component\Payment\PaymentData;
use App\Component\Payment\PaymentInterface;
use App\Component\ProductParser\ProductParserEntityCreator;
use App\Component\ProductParser\ProductParserOlx;
use App\Repository\ProductRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestCommand extends Command
{

    protected static $defaultName = 'app:test';
    /**
     * @var PaymentInterface
     */
    private $payment;
    /**
     * @var ProductParserOlx
     */
    private $olx;
    /**
     * @var ProductParserEntityCreator
     */
    private $productParserEntityCreator;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var NovaPoshtaRequest
     */
    private $novaPoshtaRequest;
    /**
     * @var CityRepository
     */
    private $cityRepository;
    /**
     * @var PostOfficeRepository
     */
    private $postOfficeRepository;

    public function __construct(
        PaymentInterface $payment,
        ProductParserOlx $olx,
        ProductParserEntityCreator $productParserEntityCreator,
        ProductRepository $productRepository,
        ContainerInterface $container,
        NovaPoshtaRequest $novaPoshtaRequest,
        CityRepository $cityRepository,
        PostOfficeRepository $postOfficeRepository,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->payment = $payment;
        $this->olx = $olx;
        $this->productParserEntityCreator = $productParserEntityCreator;
        $this->productRepository = $productRepository;
        $this->container = $container;
        $this->novaPoshtaRequest = $novaPoshtaRequest;
        $this->cityRepository = $cityRepository;
        $this->postOfficeRepository = $postOfficeRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // $data = ['modelName' => 'Address', 'calledMethod' => 'searchSettlements', 'methodProperties' => ['Limit' => $limit, 'CityName' => $name]];
        dd($this->postOfficeRepository->get('db5c888c-391c-11dd-90d9-001a92567626'));
    }

    protected function _execute(InputInterface $input, OutputInterface $output)
    {
        $url = [
            'https://www.olx.ua/obyavlenie/prodam-macbook-2015-IDGSRS0.html',
            'https://www.olx.ua/obyavlenie/prodam-zemelnu-dlyanku-dlya-budvnitstva-IDGsI2l.html',
            'https://www.olx.ua/obyavlenie/sea-view-svoya-gagarinskoe-plato-IDFducD.html',
            'https://www.olx.ua/obyavlenie/easycap-usb-karta-plata-video-zahvata-konverter-kasseta-usb-otsifrovka-IDAQVUy.html'
        ];
        $products = [];
        foreach ($url as $u){
            if (null === ($pr = $this->productRepository->findByOlxLink($u))) {
                $pr = $this->productParserEntityCreator->createByUrl($u);
                $this->productRepository->save($pr);
            }
            $products[] = $pr;
        }

        dd($products);
    }

}
