<?php


namespace App\EventListener\Doctrine;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface DoctrinePreSaveListenerInterface
{

    const TAG_NAME = 'app.doctrine.pre_save_listener';

    public function preUpdate(LifecycleEventArgs $args);
    public function prePersist(LifecycleEventArgs $args);

}
