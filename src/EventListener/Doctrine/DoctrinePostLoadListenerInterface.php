<?php


namespace App\EventListener\Doctrine;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface DoctrinePostLoadListenerInterface
{

    const TAG_NAME = 'app.doctrine.post_load_listener';

    public function postLoad(LifecycleEventArgs $args);

}
