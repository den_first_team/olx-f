<?php


namespace App\EventListener\Doctrine;


use Doctrine\ORM\Event\LifecycleEventArgs;

interface DoctrinePostSaveListenerInterface
{

    const TAG_NAME = 'app.doctrine.post_save_listener';

    public function postUpdate(LifecycleEventArgs $args);
    public function postPersist(LifecycleEventArgs $args);

}
