<?php


namespace App\EventListener\Doctrine;


use App\EventListener\Doctrine\DoctrinePostSaveListenerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

abstract class DoctrinePostSaveListenerAbstract implements DoctrinePostSaveListenerInterface
{

    /**
     * @param LifecycleEventArgs $args
     */
    final public function postUpdate(LifecycleEventArgs $args)
    {
        $this->save($args);
        $this->update($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    final public function postPersist(LifecycleEventArgs $args)
    {
        $this->save($args);
        $this->persist($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    protected function update(LifecycleEventArgs $args)
    {

    }

    /**
     * @param LifecycleEventArgs $args
     */
    protected function persist(LifecycleEventArgs $args)
    {

    }

    /**
     * @param LifecycleEventArgs $args
     * @return mixed
     */
    protected function save(LifecycleEventArgs $args)
    {

    }
}
