<?php


namespace App\EventListener\Doctrine;


use App\EventListener\Doctrine\DoctrinePreSaveListenerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

abstract class DoctrinePreSaveListenerAbstract implements DoctrinePreSaveListenerInterface
{

    /**
     * @param LifecycleEventArgs $args
     */
    final public function preUpdate(LifecycleEventArgs $args)
    {
        $this->save($args);
        $this->update($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    final public function prePersist(LifecycleEventArgs $args)
    {
        $this->save($args);
        $this->persist($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    protected function update(LifecycleEventArgs $args)
    {

    }

    /**
     * @param LifecycleEventArgs $args
     */
    protected function persist(LifecycleEventArgs $args)
    {

    }

    /**
     * @param LifecycleEventArgs $args
     * @return mixed
     */
    protected function save(LifecycleEventArgs $args)
    {

    }
}
