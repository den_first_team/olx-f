<?php


namespace App\EventListener;


use App\Entity\Product;
use App\EventListener\Doctrine\DoctrinePreSaveListenerAbstract;
use Doctrine\ORM\Event\LifecycleEventArgs;

class ProductSaveListener extends DoctrinePreSaveListenerAbstract
{

    public function persist(LifecycleEventArgs $args)
    {
       $entity = $args->getEntity();
       if (!$entity instanceof Product){
           return;
       }
       $entity->setViewCount(rand(0,1000));
    }

}
