<?php


namespace App\EventListener;


use App\Component\Product\ProductAliasGeneratorInterface;
use App\Entity\Product;
use App\EventListener\Doctrine\DoctrinePreSaveListenerAbstract;
use Doctrine\ORM\Event\LifecycleEventArgs;

class ProductGenerateAliasListener extends DoctrinePreSaveListenerAbstract
{

    /**
     * @var ProductAliasGeneratorInterface
     */
    private $productAliasGenerator;

    public function __construct(ProductAliasGeneratorInterface $productAliasGenerator)
    {
        $this->productAliasGenerator = $productAliasGenerator;
    }

    public function persist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Product){
            return;
        }
        $alias = $this->productAliasGenerator->generate($entity);
        $entity->setAlias($alias);

    }

}
