<?php


namespace App\EventListener;


use App\Component\User\AuthUserInterface;
use App\Entity\Product;
use App\EventListener\Doctrine\DoctrinePreSaveListenerAbstract;
use Doctrine\ORM\Event\LifecycleEventArgs;

class  ProductSetManagerListener extends DoctrinePreSaveListenerAbstract
{

    /**
     * @var AuthUserInterface
     */
    private $authUser;

    public function __construct(AuthUserInterface $authUser)
    {
        $this->authUser = $authUser;
    }

    public function persist(LifecycleEventArgs $args)
    {
        if (false === $this->authUser->isAuth()){
            return;
        }
        $entity = $args->getEntity();
        if (!$entity instanceof Product){
            return;
        }
        $entity->setManager($this->authUser->getUser());
    }
}
