const $ = require('jquery');


function appendCities(name){
    $.ajax(`/nova-poshta/cities?name=${name}`).done((res) => {
        let html = '';

        res.map((data) => {
            html += `
                <div class="ripple-button filter-selector  city-item" style="overflow: hidden;margin: 1px 0;" data-ref="${data.DeliveryCity}">
                <div class="filter-selector__label city-item" data-ref="${data.DeliveryCity}">${data.Present} </div>
                <div class="filter-selector__action city-item" data-ref="${data.DeliveryCity}">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8 12" color="#0198D0" style="height: 14px; width: 14px;">
                <path fill="#0198d0" fill-rule="evenodd" d="M4.6 6L0 1.4 1.4 0l2.395 2.395L7.4 6l-6 6L0 10.6z"></path>
                </svg>
                </div>
                </div>

                  `
        })
        $('#search-result').html(html)

    })
}
appendCities('')
$('#search-city-name').keyup(() => {
    let name = $('#search-city-name').val()
    appendCities(name);
})

$(document).on('click', '.city-item', (el) => {
    let cityRef = $(el.target).data('ref')
    $.ajax(`/nova-poshta/offices?cityRef=${cityRef}`).done((res) => {
        let html = '';
        res.map((data) => {
            html += `
                <div class="ripple-button filter-selector  office-item" style="overflow: hidden;margin: 1px 0;" data-ref="${data.Ref}">
                <div class="filter-selector__label office-item" data-ref="${data.Ref}">${data.Description}</div>
                <div class="filter-selector__action office-item" data-ref="${data.Ref}">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8 12" color="#0198D0" style="height: 14px; width: 14px;">
                <path fill="#0198d0" fill-rule="evenodd" d="M4.6 6L0 1.4 1.4 0l2.395 2.395L7.4 6l-6 6L0 10.6z"></path>
                </svg>
                </div>
                </div>
            `
        })
        $('#search-result').html(html)

    })
})

$(document).on('click', '.office-item', (el) => {
    let ref = $(el.target).data('ref')
    $('#post-office-input').val(ref)
    $('#post-office-form').submit()
})

